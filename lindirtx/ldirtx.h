/*
 * Copyright (c) 2017-2020, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2020/12/15
 *
 * History:
 *
 */

#ifndef __LIND_IRTX_H__
#define __LIND_IRTX_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
 * open ir-tx hardware.
 *
 * @param
 *    pwm_id    [in]: which pwm used as the 38KHz signal carrier.
 * @return
 *    =0: successed
 *    others: failed
 */
int  lindirtx_open(int pwm_id);

/**
 * close ir-tx hardware.
 *
 * @param
 * @return
 */
void lindirtx_close();

/**
 * encoder the code as NEC type, and do IR send.
 *
 * @param
 *    addr      [in]: the address code to be send.
 *    code      [in]: the command code to be send.
 *    repeat_ms [in]: send the repeat code lasts for 'repeat_ms'.
 * @return
 *    =0: successed
 *    others: failed
 */
int  lindirtx_write(unsigned char addr, unsigned char code, int repeat_ms);

#ifdef __cplusplus
}
#endif

#endif  // __LIND_IRTX_H__


