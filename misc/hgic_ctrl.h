/*
 * Copyright (c) 2017-2020, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2021/09/23
 *
 * History:
 *
 */

#ifndef __HGIC_WIFI_CTRL_H__
#define __HGIC_WIFI_CTRL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "wifi_ctrl.h"

#define MAX_STA_CNT     4

typedef enum
{
    HGIC_MSG_UNKNOWN                = 0,
    HGIC_MSG_CONNECT                = 1,
    HGIC_MSG_DISCONN                = 2,
    HGIC_MSG_SIGNAL                 = 3,
} hgic_msg_e;

typedef enum
{
    MODULE_TYPE_UNKNOWN             = 0,
    MODULE_TYPE_700M                = 1,
    MODULE_TYPE_900M                = 2,
    MODULE_TYPE_860M                = 3,
    MODULE_TYPE_810M                = 5,
} module_type_e;

typedef struct
{
    comm_msg_cb msg_cb;
    void * user;

    WIFI_MODE_e mode;

} hgicctrl_config_t;

/**
 * create a wifi(halow) controller wrapper.
 *
 * @param
 * @return
 *    >0: the wifi wrapper handle
 *    =0: failed
 */
_handle_t hgicctrl_create();

/**
 * destory the wifi controller wrapper.
 *
 * @param
 * @return
 */
void hgicctrl_destory(_handle_t h_wifi);

/**
 * initialize the wifi wrapper if need.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    p_config  [in]: the configuration.
 * @return
 *    =0: success
 *    <0: failed
 */
int hgicctrl_init(_handle_t h_wifi, hgicctrl_config_t * p_config);

/**
 * deinitialize the wifi wrapper.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 * @return
 *    =0: success
 *    <0: failed
 */
void hgicctrl_deinit(_handle_t h_wifi);

/**
 * auto pair. After pair succeed, app can get deivce ip.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    paired_mac[in/out]: the mac addr of paired device.
 *    timeout_ms[in]: sync pair until timeout.
 * @return
 *    =0: success
 *    <0: failed, -1: error, -2: paired devices more than 'MAX_PAIR_CNT'
 */
int hgicctrl_auto_pair(_handle_t h_wifi, char * paired_mac, int32_t timeout_ms);

/**
 * unpair by mac.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    mac       [in]: the mac addr of paired device.
 * @return
 *    =0: success
 *    <0: failed
 */
int hgicctrl_unpair(_handle_t h_wifi, char * mac);

/**
 * unpair by mac.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    mac   [in/out]: a pointer array to the mac addr of paired device.
 *    max_cnt   [in]: the max count of 'mac'.
 * @return
 *   >=0: the actual paired count
 *    <0: failed
 */
int hgicctrl_get_paired_mac(_handle_t h_wifi, char * mac[], const int max_cnt);

/**
 * get current wifi information.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    info  [in/out]: the current wifi information.
 * @return
 *    =0: success
 *    <0: failed
 */
int hgicctrl_get_info(_handle_t h_wifi, wifi_info_t * info);

/**
 * set the frequency and bandwidth.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    freq_start[in]: the start center frequency.
 *    freq_end  [in]: the end center frequency.
 *    bandwidth [in]: the bandwidth of this channel.
 * @return
 *    =0: success
 *    <0: failed
 */
int hgicctrl_set_freq_range(_handle_t h_wifi, int32_t freq_start, int32_t freq_end, int32_t bandwidth);

/**
 * set the frequency and bandwidth.
 *
 * @param
 *    h_wifi    [in/out]: the wifi wrapper handle.
 *    freq_start[in/out]: the start center frequency.
 *    freq_end  [in/out]: the end center frequency.
 *    bandwidth [in/out]: the bandwidth of this channel.
 * @return
 *    =0: success
 *    <0: failed
 */
int hgicctrl_get_freq_range(_handle_t h_wifi, int32_t * freq_start, int32_t * freq_end, int32_t * bandwidth);

/**
 * enable/disable auto channel switch.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    chan      [in]: the array of channel frequency.
 *    chan_count[in]: the count of 'chan' array.
 * @return
 *    =0: success
 *    <0: failed
 */
int hgicctrl_set_chan_list(_handle_t h_wifi, int32_t * chan, int32_t chan_count);

/**
 * enable/disable auto channel switch.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    enable    [in]: 1: enable, 0: disable.
 * @return
 *    =0: success
 *    <0: failed
 */
int hgicctrl_auto_chswitch(_handle_t h_wifi, int32_t enable);

/**
 * set and switch the wifi mode.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    mode      [in]: refer to WIFI_MODE_e.
 * @return
 *    =0: success
 *    <0: failed
 */
int hgicctrl_set_mode(_handle_t h_wifi, WIFI_MODE_e mode);

/**
 * use outside 1.3V DCDC to reduce the power consumption.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    outside   [in]: 1: use outside 1.3V DCDC, default: 0.
 * @return
 *    =0: success
 *    <0: failed
 */
int hgicctrl_set_dcdc13(_handle_t h_wifi, int32_t outside);

/**
 * set and switch the wifi mode.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    outside   [in]: 1: use outside 1.3V DCDC, default: 0.
 * @return
 *      1: 700M 2: 900M 3: 860M 4: 810M
 *    <=0: failed
 */
module_type_e hgicctrl_get_module_type(_handle_t h_wifi);

/**
 * switch(if need) wifi to softap mode, and set the softap name and password specified by 'ssid' and 'pw'.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    ssid      [in]: the ssid of AP.
 *    pw        [in]: the password of AP.
 * @return
 *    =0: success
 *    <0: failed
 */
int hgicctrl_ap_set_attr(_handle_t h_wifi, const char * ssid, const char * pw);

/**
 * get the STA list.
 *
 * @param
 *    h_wifi        [in]: the wifi wrapper handle.
 *    sta_list  [in/out]: a pointer to a 'wifi_info_t' struct array.
 *    max_cnt       [in]: max count of 'sta_list'.
 * @return
 *    >0: the STA list count
 *   <=0: failed
 */
int hgicctrl_ap_get_sta_list(_handle_t h_wifi, wifi_info_t sta_list[], int max_cnt);

#ifdef __cplusplus
}
#endif

#endif  // __HGIC_WIFI_CTRL_H__

