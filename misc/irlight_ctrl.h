/*
 * Copyright (c) 2017-2021, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2021/01/22
 *
 * History:
 *
 */

#ifndef __LIND_IR_LIGHT_H__
#define __LIND_IR_LIGHT_H__

#ifdef __cplusplus
extern "C" {
#endif

int irlight_init();
void irlight_deinit();
void irlight_auto_enable();
void irlight_auto_disable();
void irlight_on();
void irlight_off();
int  ldr_get_value();

void power_led_flashing(int time_ms);
void power_led_on();
void power_led_off();

#ifdef __cplusplus
}
#endif

#endif  // __LIND_IR_LIGHT_H__


