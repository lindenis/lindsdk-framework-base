/*
 * Copyright (c) 2017-2020, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2020/10/30
 *
 * History:
 *
 */

#ifndef __DISK_CTRL_H__
#define __DISK_CTRL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "osal_types.h"

typedef enum _DISK_TYPE_e
{
    DISK_TYPE_INTERNEL          = 0,
    DISK_TYPE_EXTSD             = 1,
    DISK_CNT
} DISK_TYPE_e;

typedef enum
{
    DISK_MSG_NULL               = 0,
    DISK_MSG_VOLUME_FULL        = 1,
} DISK_MSG_e;

/*
 * disk information
 */
typedef struct _disk_info_t
{
    int     mounted;
    char *  path;

    /*
     * size in MB
     */
    int     total;
    int     free;

    int     inode;
    int     ifree;
} disk_info_t;

typedef struct _diskctrl_config_t
{
    comm_msg_cb msg_cb;
    void * user;

    /*
     * "vfat" "fat32" "ext4"
     */
    char fs_format[16];

    /*
     * the disk to be monitored
     */
    DISK_TYPE_e disk;

    /*
     * notify volume usage in every 'percent_step'.
     * If it is '0', do not notify.
     */
    int percent_step;

    /*
     * volume uesd percent.
     * If the disk volume is used larger than this value,
     * it will notify a message.
     * If it is '0', do not notify.
     */
    int warning_percent;

    /*
     * disk free size in MB.
     * If the disk free size is smaller than this value,
     * it will notify a message.
     * If it is '0', do not notify.
     */
    int warning_free;
} diskctrl_config_t;

_handle_t diskctrl_create();
void diskctrl_destory(_handle_t h_disk);

int diskctrl_init(_handle_t h_disk, diskctrl_config_t * p_config);
int diskctrl_deinit(_handle_t h_disk);

int diskctrl_get_info(_handle_t h_disk, DISK_TYPE_e disk, disk_info_t * info);
int diskctrl_format(_handle_t h_disk, DISK_TYPE_e disk);
int diskctrl_mount(_handle_t h_disk, DISK_TYPE_e disk, char * node);
int diskctrl_umount(_handle_t h_disk, DISK_TYPE_e disk);
int diskctrl_try_mount(_handle_t h_disk, DISK_TYPE_e disk);

#ifdef __cplusplus
}
#endif

#endif  // __DISK_CTRL_H__

