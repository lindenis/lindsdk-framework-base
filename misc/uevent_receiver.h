/*
 * Copyright (c) 2017-2020, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2020/11/18
 *
 * History:
 *
 */

#ifndef __UEVENT_RECEIVER_H__
#define __UEVENT_RECEIVER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "osal_types.h"

typedef enum
{
    UEVENT_MSG_NULL                 = 0,
    UEVENT_MSG_MOTOR_STOPPED        = 1,
    UEVENT_MSG_LIMIT_SWITCH         = 2,
    UEVENT_MSG_DISK_PLUGIN          = 3,
    UEVENT_MSG_DISK_PLUGOUT         = 4,
    UEVENT_MSG_DISK_CHANGED         = 5,    // after disk format success, will get this msg
} UEVENT_MSG_e;

typedef struct _uevent_config_t
{
    comm_msg_cb     msg_cb;
    void *          user;

} uevent_config_t;

_handle_t uevent_create();
void uevent_destory(_handle_t h_uevent);
int uevent_init(_handle_t h_uevent, uevent_config_t * p_config);
int uevent_deinit(_handle_t h_uevent);

#ifdef __cplusplus
}
#endif

#endif  // __UEVENT_RECEIVER_H__

