/*
 * Copyright (c) 2017-2020, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2020/11/02
 *
 * History:
 *
 */

#ifndef __WIFI_CTRL_H__
#define __WIFI_CTRL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "osal_types.h"

typedef enum _WIFI_MODE_e
{
    WIFI_MODE_STATION               = 0x1,
    WIFI_MODE_SOFTAP                = 0x2,
    WIFI_MODE_P2P                   = 0x4,
} WIFI_MODE_e;

typedef enum _WIFI_STATUS_e
{
    WIFI_STATUS_UNKNOWN             = -1,
    WIFI_STATUS_DISCONNECTED        = 0,
    WIFI_STATUS_CONNECTED,
    WIFI_STATUS_UNREACHABLE,
    WIFI_STATUS_CNT
} WIFI_STATUS_e;

typedef enum WIFI_SECURITY_e
{
    WIFI_SECURITY_OPEN              = 0,
    WIFI_SECURITY_WEP,
    WIFI_SECURITY_WPA_WPA2_EAP,
    WIFI_SECURITY_WPA_WPA2_PSK,
    WIFI_SECURITY_WAPI_CERT,
    WIFI_SECURITY_WAPI_PSK,
    WIFI_SECURITY_BOTTON
} WIFI_SECURITY_e;

/*
 * wifi information
 */
typedef struct _wifi_ap_info_t
{
    char            ssid[32];
    int             frequency;
    char            password[32];
    char            mac[18];
    int             quality;
    int             rssi;
    WIFI_SECURITY_e security;
    int             is_saved;   // 0: not saved    1: saved
    char            netid;      // when is_saved = 1, it have netid in wpa_supplicant.conf
} wifi_ap_info_t;

/*
 * wifi information
 */
typedef struct _wifi_info_t
{
    WIFI_MODE_e     mode;
    WIFI_STATUS_e   status;

    char            ssid[32];
    int             quality;
    int             rssi;

    /*
     * format: 192.168.1.10
     */
    char            ip[16];

    /*
     * format: 0c:c6:55:2e:0a:75
     */
    char            mac[18];

    /*
     * p2p attibution
     */
} wifi_info_t;

typedef struct _wifictrl_config_t
{
    comm_msg_cb msg_cb;
    void * user;

} wifictrl_config_t;

/**
 * create a wifi controller wrapper.
 *
 * @param
 * @return
 *    >0: the wifi wrapper handle
 *    =0: failed
 */
_handle_t wifictrl_create();

/**
 * destory the wifi controller wrapper.
 *
 * @param
 * @return
 */
void wifictrl_destory(_handle_t h_wifi);

/**
 * initialize the wifi wrapper if need.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    p_config  [in]: the configuration.
 * @return
 *    =0: success
 *    <0: failed
 */
int wifictrl_init(_handle_t h_wifi, wifictrl_config_t * p_config);

/**
 * deinitialize the wifi wrapper.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 * @return
 *    =0: success
 *    <0: failed
 */
int wifictrl_deinit(_handle_t h_wifi);

/**
 * get current wifi information.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    info  [in/out]: the current wifi information.
 * @return
 *    =0: success
 *    <0: failed
 */
int wifictrl_get_info(_handle_t h_wifi, wifi_info_t * info);

/**
 * wifi scan to get the AP list.
 *
 * @param
 *    h_wifi        [in]: the wifi wrapper handle.
 *    ap_list   [in/out]: a pointer to a 'wifi_ap_info_t' struct array.
 * @return
 *    >0: the AP list count
 *   <=0: failed
 */
int wifictrl_get_ap_list(_handle_t h_wifi, wifi_ap_info_t ap_list[], int max_cnt);

/**
 * set and switch the wifi mode.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    mode      [in]: refer to WIFI_MODE_e.
 * @return
 *    =0: success
 *    <0: failed
 */
int wifictrl_set_mode(_handle_t h_wifi, WIFI_MODE_e mode);

/**
 * switch(if need) wifi to station mode, and connect to an AP specified by 'ssid' and 'pw'.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    ssid      [in]: the ssid of AP.
 *    pw        [in]: the password of AP.
 *    is_saved  [in]: whether save of AP.
 *    netid     [in]: the netid of saved AP.
 * @return
 *    =0: success
 *    <0: failed
 */
int wifictrl_set_sta_connect(_handle_t h_wifi, const char * ssid, const char * pw, int is_saved, uint8_t netid);

/**
 * switch(if need) wifi to softap mode, and set the softap name and password specified by 'ssid' and 'pw'.
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    ssid      [in]: the ssid of AP.
 *    pw        [in]: the password of AP.
 * @return
 *    =0: success
 *    <0: failed
 */
int wifictrl_set_ap_attr(_handle_t h_wifi, const char * ssid, const char * pw);

/**
 * delete net saved in "/etc/wifi/wpa_supplicant.conf".
 *
 * @param
 *    h_wifi    [in]: the wifi wrapper handle.
 *    ssid      [in]: the ssid to delete, if ssid is 'null', delete all net.
 * @return
 *    =0: success
 *    <0: failed
 */
int wifictrl_remove_net(_handle_t h_wifi, const char * ssid);

#ifdef __cplusplus
}
#endif

#endif  // __WIFI_CTRL_H__

