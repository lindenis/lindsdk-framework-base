/*
 * Copyright (c) 2017-2021, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      leo@lindeni.com
 *
 * Create Date:
 *      2021/04/22
 *
 * History:
 *
 */

#ifndef __MOTION_DECTION_H__
#define __MOTION_DECTION_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "osal_types.h"

typedef struct _motion_config_t
{
    int         width;
    int         height;
    int         num_sam;
    int         min_match;
    int         r;
    int         rand_sam;
    comm_msg_cb msg_cb;
    void *      user;
} motion_config_t;

typedef enum __motion_msg_e {
    MOTION_INVALID      = 0,
    MOTION_TRIGGER      = 1,
} motion_msg_e;

_handle_t motion_create();
void motion_destroy(_handle_t h_motion);

// Init Background Model.
void motion_init(_handle_t h_motion, motion_config_t * config);

// Process First Frame of Video Query
void motion_process_first_frame(_handle_t h_motion, uint8_t * data);

// Run the Algorithm: Extract Foreground Areas & Update Background Model Sample Library.
int motion_run(_handle_t h_motion, uint8_t * data);

// Delete Sample Library.
void motion_delete_samples(_handle_t h_motion);

#ifdef __cplusplus
}
#endif

#endif  // __MOTION_DECTION_H__
