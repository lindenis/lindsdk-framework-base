/*
 * Copyright (c) 2017-2021, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2021/01/04
 *
 * History:
 *
 */

#ifndef __LIND_ZERO_CONFIG_H__
#define __LIND_ZERO_CONFIG_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "osal_types.h"

/*
 * MTU, UDP
 * 'data' size max 'MAX_PKG_SIZE - 4'
 */
#define MAX_PKG_SIZE    1472

typedef enum _zcfg_srv_e
{
    /**
     * query or notify a rtsp server
     */
    ZCFG_SRV_RTSP_URL               = 0x0000,
    ZCFG_SRV_RTSP_URL_EXIT          = 0x0001,

    /**
     * lindtunnel session initiation protocol
     */
    ZCFG_SRV_LINDTUNNEL_SIP         = 0x0002,
    ZCFG_SRV_LINDTUNNEL_SIP_EXIT    = 0x0003,

    /**
     * wifi halow private protocol: obtain an IP address automatically
     */
    ZCFG_SRV_DHCP                   = 0x0004,
    ZCFG_SRV_DHCP_EXIT              = 0x0005,

    ZCFG_SRV_FACTORY                = 0xFFF0,
    ZCFG_SRV_FACTORY_EXIT           = 0xFFF1,
    ZCFG_SRV_INVALID                = 0xFFFF
} zcfg_srv_e;

/*
 * ZCFG_SRV_DHCP
 */
typedef struct
{
    char mac[32];
    char ip[16];
} zcfg_dhcp_t;

typedef enum _zcfg_cast_mode_e
{
    ZCFG_BROADCAST      = 0,
    ZCFG_MULTICAST,
    ZCFG_UNICAST,               // for clients listen to the server
} zcfg_cast_mode_e;

typedef struct _zcfg_config_t
{
    int (* cb)(void * user, int32_t srv, void * data, int size);
    void * user;

    char * xcast_ip;
    zcfg_cast_mode_e xcast_mode;
} zcfg_config_t;

/*
 * zcfg client api
 * such as the player app find the rtsp server url.
 */
int  zcfg_cli_init(_handle_t * p_zcfg, zcfg_config_t * p_config);
void zcfg_cli_deinit(_handle_t h_zcfg);
int  zcfg_cli_query(_handle_t h_zcfg, int32_t srv, uint8_t * data, int size);

/*
 * zcfg server api
 * the service provider regist themself for client querying.
 */
int  zcfg_srv_init(_handle_t * p_zcfg, zcfg_config_t * p_config);
void zcfg_srv_deinit(_handle_t h_zcfg);
int  zcfg_srv_register(_handle_t h_zcfg, int32_t srv, uint8_t * data, int size);
int  zcfg_srv_unregister(_handle_t h_zcfg, int32_t srv, uint8_t * data, int size);

#ifdef __cplusplus
}
#endif

#endif  // __LIND_ZERO_CONFIG_H__

