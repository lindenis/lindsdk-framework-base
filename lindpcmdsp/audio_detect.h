/*
 * Copyright (c) 2017-2020, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2021/04/28
 *
 * History:
 *
 */

#ifndef __AUDIO_DETECT_H__
#define __AUDIO_DETECT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "osal_types.h"

typedef enum __audio_detect_msg_e {
    AUDIO_DETECT_INVALID            = 0,
    AUDIO_DETECT_BABYCRY_START      = 1,
    AUDIO_DETECT_BABYCRY_STOP       = 2,
    AUDIO_DETECT_SOUND_TRIGGER      = 3,
    AUDIO_DETECT_CONTINUOUS_START   = 4,
    AUDIO_DETECT_CONTINUOUS_STOP    = 5,
    AUDIO_DETECT_GET_DB             = 6,
} audio_event_e;

typedef struct
{
    int8_t mode;            // 0: off, 1: on
    int8_t sensitivity;     // range: [0, 100]
    int8_t reserved[2];
} alarm_sound_t;

typedef struct
{
    /*
     * range: 0~100, [0, 10], [0, 1]
     */
    int                     sensitivity;

    /*
     * unit: second
     */
    int                     alarm_interval;

    int                     enable_get_db;

    comm_msg_cb             msg_cb;
    void *                  user;
} audio_detect_config_t;

_handle_t audio_detect_create();
void audio_detect_destory(_handle_t h_adetc);
int audio_detect_init(_handle_t h_adetc, audio_detect_config_t * p_config);
int audio_detect_deinit(_handle_t h_adetc);

int audio_detect_send_pcm(_handle_t h_adetc, uint8_t * pcm, int size);
void audio_detect_set_sensitivity(_handle_t h_adetc, int sensitivity);
int audio_detect_set_alarm_interval(_handle_t h_adetc, int seconds);

/*
 * distance unit: cm
 */
void audio_detect_send_distance(_handle_t h_adetc, int distance);

#ifdef __cplusplus
}
#endif

#endif  // __AUDIO_DETECT_H__

