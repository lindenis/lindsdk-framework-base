/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      leo@lindeni.com
 *
 * Create Date:
 *      2021/03/06
 *
 * History:
 *
 */
#ifndef __LIND_PCMDSP_H__
#define __LIND_PCMDSP_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <osal_types.h>

#define MAKEFOURCC(ch0, ch1, ch2, ch3) \
        (((unsigned int)(ch0)) | (((unsigned int)(ch1)) << 8) | \
        (((unsigned int)(ch2)) << 16) | (((unsigned int)(ch3)) << 24))

#define ID_RIFF MAKEFOURCC('R', 'I', 'F', 'F')
#define ID_WAVE MAKEFOURCC('W', 'A', 'V', 'E')
#define ID_DATA MAKEFOURCC('d', 'a', 't', 'a')
#define ID_FMT  MAKEFOURCC('f', 'm', 't', '\x20')
#define ID_FACT MAKEFOURCC('f', 'a', 'c', 't')

#define WAVE_FORMAT_PCM 0x01
#define FFT_SPEED       0.06

typedef struct _RIFF_chunk_t {
    uint32_t chunk_id;
    uint32_t chunk_size;
    uint32_t format;
}RIFF_format_t;

typedef struct _fmt_sub_chunk_t {
    uint32_t sub_chunk_id;
    uint32_t sub_chunk_size;
    uint16_t audio_format;
    uint16_t num_channels;
    uint32_t sample_rate;
    uint32_t byte_rate;
    uint16_t bloc_align;
    uint16_t bits_per_sample;
    uint16_t cb_size;
}fmt_sub_chunk_t;

typedef struct _data_sub_chunk_t {
    uint32_t data_id;
    uint32_t data_size;
    uint8_t* data;
}data_sub_chunk_t;

typedef struct _lindpcmdsp_config_t {
    unsigned int bits;
    unsigned int period_size;
    unsigned int channels;
}lindpcmdsp_config_t;

int lindpcmdsp_readwav(const char * path, RIFF_format_t * head, fmt_sub_chunk_t * fmt, data_sub_chunk_t * data);
int lindpcmdsp_calculate_db(const char * data, int size, int bits);
int lindpcmdsp_calculate_rms(const char * data, int size, int bits);
int lindpcmdsp_calculate_amp(const char * data, int size, int bits);

#ifdef USE_FFT
_handle_t lindpcmdsp_fft_create();
void lindpcmdsp_fft_destory(_handle_t h_lindpcmdsp);
int lindpcmdsp_fft_open(_handle_t h_lindpcmdsp, lindpcmdsp_config_t config);
int lindpcmdsp_fft_close(_handle_t h_lindpcmdsp);
int lindpcmdsp_fft_process(_handle_t h_lindpcmdsp, const char * data,  int * amp);
#endif

#ifdef __cplusplus
}
#endif

#endif  // __LIND_PCMDSP_H__