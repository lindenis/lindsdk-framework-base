#ifndef _INPUT_EVENT_H_
#define _INPUT_EVENT_H_

#include <linux/input.h>

#ifdef __cplusplus
extern "C" {
#endif

void RegisterInputEventHandler(int code, void *(*handler)(void *), void *args);
void RegisterInputRepeatEventHandler(int code, void *(*handler)(void *), void *args, unsigned int timeout);
int StartInputEventLoop(void);

#ifdef __cplusplus
}
#endif

#endif /* _INPUT_EVENT_H_ */
