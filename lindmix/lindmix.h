/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      leo@lindeni.com
 *
 * Create Date:
 *      2021/03/06
 *
 * History:
 *
 */
#ifndef __LIND_MIX_H__
#define __LIND_MIX_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "osal_types.h"

#define LINDMIX_INI_FILE_PATH "/etc/lindmix.ini"

typedef enum _lindmix_control_e
{
    LINDMIX_LINEOUT_VOLUME = 0,
    LINDMIX_DIGITAL_VOLUME,
    LINDMIX_MIC1_AMP_GAIN_CONTROL,
    RADC_INPUT_MIXER_LINEINR_SWITCH,
    LADC_INPUT_MIXER_LINEINR_SWITCH,
    RADC_INPUT_MIXER_MIC1_BOOT_SWITCH,
    LADC_INPUT_MIXER_MIC1_BOOT_SWITCH,
    AIF1_AD0L_MIXER_ADCL_SWITCH,
    AIF1_AD0R_MIXER_ADCR_SWITCH,
    AIF1OUT0L_MUX,
    AIF1OUT0R_MUX,
    AIF1IN0L_MUX,
    AIF1IN0R_MUX,
    LINEOUT_SWITCH,
    LINEOUTL_MUX,
    LINEOUTR_MUX,
    DACL_MIXER_AIF1DA0L_SWITCH,
    DACR_MIXER_AIF1DA0R_SWITCH,
    LEFT_OUTPUT_MIXER_DACL_SWITCH,
    RIGHT_OUTPUT_MIXER_DACL_SWITCH,
    MIC1_BOOST_AMPLIFIER_GAIN,
    ADC3_INPUT_MIC3_BOOST_SWITCH,
    HEADPHONE_SWITCH,
    HPSPEAKER_SWITCH,
    EXTERNAL_SPEAKER_SWITCH,
    LEFT_INPUT_MIXER_MIC1_BOOST_SWITCH
} lindmix_control_e;

typedef enum _lindmix_path_e
{
    PCM_CAPTURE_LINEIN = 0,
    PCM_CAPTURE_MIC,
    PCM_PLAYBACK_HEADPHONE,
    PCM_PLAYBACK_SPEAKER
} lindmix_path_e;

typedef enum _lindmix_device_e
{
    LINDMIX_DEVICE_CODEC = 0,
    LINDMIX_DEVICE_HDMI  = 1,
} lindmix_device_e;

_handle_t lindmix_create();
void lindmix_destory(_handle_t h_lindmix);
int lindmix_set_device(_handle_t h_lindmix, lindmix_device_e device);
int lindmix_open(_handle_t h_lindmix);
int lindmix_close(_handle_t h_lindmix);
int lindmix_set_value(_handle_t h_lindmix, lindmix_control_e control_e, int value1, int value2);
int lindmix_get_range_max(_handle_t h_lindmix, lindmix_control_e control_e);

/*
 * value: 0 ~ 100
 */
int lindmix_set_volume(lindmix_device_e device, int value);

/*
 * value: 0 ~ 100
 */
int lindmix_set_mic_volume(lindmix_device_e device, int value);

void lindmix_load_path(lindmix_path_e path);

#ifdef __cplusplus
}
#endif

#endif  // __LIND_MIX_H__