/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2019/08/08
 *
 * History:
 *
 */

#ifndef __OSAL_COMMON_H__
#define __OSAL_COMMON_H__

#ifdef __cplusplus
extern "C" {
#endif

#define ALIGN_8(x)    ((x + 07) & (~07))
#define ALIGN_16(x)   ((x + 15) & (~15))
#define ALIGN_32(x)   ((x + 31) & (~31))
#define ALIGN_64(x)   ((x + 63) & (~63))

#ifndef max
    #define max(a,b)	((a)>(b)?(a):(b))
#endif

#ifndef min
    #define min(a,b)	((a)<(b)?(a):(b))
#endif

/**
 * get local datetime in unit us.
 *
 * @param
 * @return
 */
long long getNowUs(void);

/**
 * get tickcount from power on in unit us.
 *
 * @param
 * @return
 */
long long get_tick_us(void);
long long get_tick_ms(void);

/**
 * get current error.
 *
 * @param
 * @return
 *    the error description
 */
char * getError(void);

/**
 * wait an input char from the terminal.
 *
 * @param
 *    ch[out]: the char got from the terminal.
 *    time_out_us[in]: timeout value in unit: us
 * @return
 *    >0: got a char
 *     0: timeout
 *    -1: error
 */
int wait_input_char(signed char * ch, long long time_out_us);

/**
 * execute the system command.
 *
 * @param
 *    command[in]: the command will be executed.
 *    result[out]: the result of command executed.
 * @return
 */
void system_cmd(const char * command, char * result);
int system_cmd_find_key(const char * command, char * key);
int system_cmd_get_keyvalue(const char * command, char * key, char * value);

/**
 * write an buffer to a file.
 *
 * @param
 *    filepath[in]: the file path.
 *    buf     [in]: the buf will be written.
 *    bufsize [in]: size of 'buf'.
 * @return
 *     0: success
 *    -1: failed
 */
int save_file(char * filepath, void * buf, int bufsize);

/**
 * read data from an file.
 *
 * @param
 *    filepath[in]: the file path.
 *    buf     [in]: the buf will be read to.
 *    bufsize [in]: the max size of 'buf'.
 * @return
 *     0: success
 *    -1: failed
 */
int read_file(char * filepath, void * buf, int bufsize);

/**
 * get the length of file.
 *
 * @param
 *    filepath[in]: the file path.
 * @return
 *    >0: the length of file
 *   <=0: error
 */
int file_size(char * filepath);

/**
 * get the hardware platform, such as "sun8i".
 *
 * @param
 *    hardware [in/out]: an char array, the function will fill in the platform name.
 * @return
 *    0: success
 *   <0: failed
 */
int get_hardware(char * hardware);

/**
 * dump an buffer in Hexadecimal.
 *
 * @param
 *    data [in]: the data to be print out.
 *    size [in]: the size of 'data'.
 *    align[in]: print number byte of a line.
 * @return
 */
void dump_hex(unsigned char * data, int size, int align);

/**
 * format the datetime to a string like this: "2020-11-2 Mon 10:41:28"
 *
 * @param
 *    buf [in/out]: the datetime format to 'buf'.
 *    max_size [in]: the max size of 'buf'.
 *    weekday[in]: wether format the weekday to the string.
 * @return
 */
void get_datetime(char * buf, int max_size, int weekday);

/**
 * get local ip/mac address of the net interface
 *
 * @param
 *    which [in] : the net interface, such as 'eth0', 'wlan0'.
 *    ip/mac[out]: ip/mac address.
 * @return
 *    0: success
 *   <0: failed
 */
int get_net_info(const char * net_inf, char * mac, char * ip,
                        char * mask, char * bcast, char * gate);
void dump_net_info(void);
int get_local_ip(const char * net_inf, char * ip);
int get_broad_ip(const char * net_inf, char * ip);
int get_net_mask(const char * net_inf, char * ip);
int get_local_mac(const char * net_inf, char * mac);
int get_default_ip(char * ip);
int get_default_broad_ip(char * ip);
int get_default_net_mask(char * ip);
int get_default_mac(char * mac);

/**
 * converted the string to lower/upper
 *
 * @param
 *    src [in]: the source string.
 *    dst [in]: the destination lower/upper string.
 * @return
 */
void str2lower(const char * src, char * dst);
void str2upper(const char * src, char * dst);


#ifdef __cplusplus
}
#endif

#endif  // __OSAL_COMMON_H__
