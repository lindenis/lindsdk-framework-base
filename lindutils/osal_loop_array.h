/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2020/07/13
 *
 * History:
 *
 */

#ifndef __LOOP_ARRAY_MANAGER_H__
#define __LOOP_ARRAY_MANAGER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "osal_mutex.h"
#include "osal_types.h"

_handle_t loop_array_create(int max);
void loop_array_destroy(_handle_t h_arr);
int loop_array_try_pop(_handle_t h_arr);
void loop_array_popped(_handle_t h_arr);
int loop_array_try_push(_handle_t h_arr);
void loop_array_pushed(_handle_t h_arr);
void loop_array_dump(_handle_t h_arr);

#ifdef __cplusplus
}
#endif

#endif  // __LOOP_ARRAY_MANAGER_H__

