/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2019/08/08
 *
 * History:
 *
 */

#ifndef __OSAL_CONDITION__
#define __OSAL_CONDITION__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <pthread.h>

#include "osal_mutex.h"

#define OSAL_malloc     malloc
#define OSAL_free       free
#define OSAL_SetError   printf

#define ErrorNone       0
#define ErrorUnknown   -1

typedef struct OSAL_cond_t
{
    pthread_cond_t cond;
} OSAL_cond;

OSAL_cond * OSAL_CreateCond(void);
void        OSAL_DestroyCond(OSAL_cond * cond);
int         OSAL_CondSignal(OSAL_cond * cond);
int         OSAL_CondBroadcast(OSAL_cond * cond);
int         OSAL_CondWaitTimeout(OSAL_cond * cond, OSAL_mutex * mutex, unsigned int ms);
int         OSAL_CondWait(OSAL_cond * cond, OSAL_mutex * mutex);


#ifdef __cplusplus
}
#endif

#endif	// __OSAL_CONDITION__

