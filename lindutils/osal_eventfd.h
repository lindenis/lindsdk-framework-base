/*
 * Copyright (c) 2017-2021, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2021/8/14
 *
 * History:
 *
 */

#ifndef __OSAL_EVENT_FD_H__
#define __OSAL_EVENT_FD_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <stdio.h>

#include "osal_types.h"

_handle_t ld_eventfd_create(unsigned int initval, int flags);
void ld_eventfd_destroy(_handle_t h_fd);
int ld_eventfd_getfd(_handle_t h_fd);
int ld_eventfd_read(_handle_t h_fd, int * cnt);
int ld_eventfd_write(_handle_t h_fd, int cnt);

#ifdef __cplusplus
}
#endif

#endif  // __OSAL_EVENT_FD_H__

