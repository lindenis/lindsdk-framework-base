/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2020/9/11
 *
 * History:
 *
 */

#include "osal_log.h"
#include "osal_prop.h"

extern char g_log_level;

#ifdef LOG_TYPE_ELOG
extern void ld_log_init(char * file_name, size_t max_size, int max_rotate);
#ifndef ELOG_FILE_NAME
/* EasyLogger file log plugin's using file name */
#define ELOG_FILE_NAME      "/tmp/elog_file.log"
#endif

#ifndef ELOG_FILE_MAX_SIZE
/* EasyLogger file log plugin's using file max size */
#define ELOG_FILE_MAX_SIZE  (512 * 1024)
#endif

#ifndef ELOG_FILE_MAX_ROTATE
/* EasyLogger file log plugin's using max rotate file count */
#define ELOG_FILE_MAX_ROTATE 5
#endif
#endif

void __attribute__ ((constructor)) osal_lib_init(void)
{
#ifdef WIN32
    ldprop_init("lindsdk.ini");
#else
    ldprop_init(NULL);
#endif
    g_log_level = ldprop_get_int(SECTION_LINDSDK, "log_level", LOG_LVL_DEBUG);
#ifdef LOG_TYPE_ELOG
    char log_name[128] = {0};
    int log_max_size = 0;
    int log_max_rotate = 0;

    strcpy(log_name, ldprop_get_string(SECTION_LINDSDK, "log_file_name", ELOG_FILE_NAME));
    log_max_size = ldprop_get_int(SECTION_LINDSDK, "log_file_max_size", ELOG_FILE_MAX_SIZE);
    log_max_rotate = ldprop_get_int(SECTION_LINDSDK, "log_file_max_rotate", ELOG_FILE_MAX_ROTATE);
    ld_log_init(log_name, log_max_size, log_max_rotate);
    printf("log parameters: %s, %d, %d\n", log_name, log_max_size, log_max_rotate);
#endif
}

void __attribute__ ((destructor)) osal_lib_deinit(void)
{
    ldprop_deinit();
}

