/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2021/04/13
 *
 * History:
 *
 */

#ifndef __OSAL_MESSAGE__
#define __OSAL_MESSAGE__

#ifdef __cplusplus
extern "C" {
#endif

#include "osal_types.h"

typedef int (*on_msg_fn)(void * user, int msg, void * data, int size);

_handle_t   osal_msg_create(int max, char * msg_name, void * user, on_msg_fn on_msg);
void        osal_msg_destroy(_handle_t h_mq);
int         osal_msg_post(_handle_t h_mq, int msg, void * data, int size);
int         osal_msg_send(_handle_t h_mq, int msg, void * data, int size);
int         osal_msg_get(_handle_t h_mq,  int * msg, void * data, int size, int timeout_ms);
int         osal_msg_get_nb(_handle_t h_mq);
void        osal_msg_flush(_handle_t h_mq, int free_msg);

#ifdef __cplusplus
}
#endif

#endif	// __OSAL_MESSAGE__

