/*
 * Copyright (c) 2017-2021, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2021/12/13
 *
 * History:
 *
 */

#ifndef __OSAL_ALLOC_H__
#define __OSAL_ALLOC_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <stdio.h>

void *osal_malloc(size_t size);
void *osal_calloc(int num, size_t size);
void *osal_realloc(void *ptr, size_t size);
void  osal_free(void *ptr);

#ifdef __cplusplus
}
#endif

#endif  // __OSAL_ALLOC_H__

