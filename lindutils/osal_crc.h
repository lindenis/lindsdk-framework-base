/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2020/01/02
 *
 * History:
 *
 */

#ifndef __OSAL_CRC_H_
#define __OSAL_CRC_H_

#ifdef __cplusplus
extern "C" {
#endif

uint16_t crc16(uint8_t *buffer, uint16_t buffer_length);

#ifdef __cplusplus
}
#endif

#endif  // __OSAL_CRC_H_


