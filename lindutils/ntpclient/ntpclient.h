/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2020/11/17
 *
 * History:
 *
 */

#ifndef __NTP_CLIENT_H__
#define __NTP_CLIENT_H__

#ifdef __cplusplus
extern "C" {
#endif

int ntpclient( int argc, char* argv[ ] );

#ifdef __cplusplus
}
#endif

#endif // __NTP_CLIENT_H__


