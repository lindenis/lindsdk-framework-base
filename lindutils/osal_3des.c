#include <stdio.h>
#include <string.h>

#include "osal_3des.h"

ElemType S[4][4][16] =
         //S1
        {{{14,4,13,1,2,15,11,8,3,10,6,12,5,9,0,7},
          {0,15,7,4,14,2,13,1,10,6,12,11,9,5,3,8},
          {4,1,14,8,13,6,2,11,15,12,9,7,3,10,5,0},
          {15,12,8,2,4,9,1,7,5,11,3,14,10,0,6,13}},
         //S2
         {{15,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10},
          {3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5},
          {0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15},
          {13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9}},
         //S3
         {{10,0,9,14,6,3,15,5,1,13,12,7,11,4,2,8},
          {13,7,0,9,3,4,6,10,2,8,5,14,12,11,15,1},
          {13,6,4,9,8,15,3,0,11,1,2,12,5,10,14,7},
          {1,10,13,0,6,9,8,7,4,15,14,3,11,5,2,12}},
         //S4
         {{7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15},
          {13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9},
          {10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4},
          {3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14}}};

ElemType S1[4][4][16]=
         //S5
        {{{2,12,4,1,7,10,11,6,8,5,3,15,13,0,14,9},
          {14,11,2,12,4,7,13,1,5,0,15,10,3,9,8,6},
          {4,2,1,11,10,13,7,8,15,9,12,5,6,3,0,14},
          {11,8,12,7,1,14,2,13,6,15,0,9,10,4,5,3}},
          //S6
         {{12,1,10,15,9,2,6,8,0,13,3,4,14,7,5,11},
          {10,15,4,2,7,12,9,5,6,1,13,14,0,11,3,8},
          {9,14,15,5,2,8,12,3,7,0,4,10,1,13,11,6},
          {4,3,2,12,9,5,15,10,11,14,1,7,6,0,8,13}},
          //S7
         {{4,11,2,14,15,0,8,13,3,12,9,7,5,10,6,1},
          {13,0,11,7,4,9,1,10,14,3,5,12,2,15,8,6},
          {1,4,11,13,12,3,7,14,10,15,6,8,0,5,9,2},
          {6,11,13,8,1,4,10,7,9,5,0,15,14,2,3,12}},
          //S8
         {{13,2,8,4,6,15,11,1,10,9,3,14,5,0,12,7},
          {1,15,13,8,10,3,7,4,12,5,6,11,0,14,9,2},
          {7,11,4,1,9,12,14,2,0,6,10,13,15,3,5,8},
          {2,1,14,7,4,10,8,13,15,12,9,0,3,5,6,11}}};

//将长度为8的字符串转为二进制位串
char Char8ToBit64(ElemType ch[8],ElemType *bit_a)
{
    ElemType cnt,cnt1;
    for(cnt = 0; cnt < 8; cnt++){
        for(cnt1 = 0;cnt1 < 8; cnt1++)
        {
            if((ch[cnt]& 0x80) == 0x80)
                *bit_a = 0x01;
            else
                *bit_a = 0x00;
            bit_a++;
            ch[cnt]=ch[cnt]<<1;
        }
    }
    return 0;
}

//将二进制位串转为长度为8的字符串
void Bit64ToChar8(ElemType bit_a[64],ElemType ch[8])
{
    unsigned char cnt,cnt1;
    memset(ch,0,8);
    for(cnt = 0; cnt < 8; cnt++)
    {
        for(cnt1 = 0;cnt1 < 8; cnt1++)
        {
            ch[cnt]=ch[cnt]<<1;
            ch[cnt]|= *bit_a;
            bit_a++ ;
        }
    }
}

//密钥置换1
void DES_PC1_Transform(ElemType key[64], ElemType tempbts[56])
{
    ElemType cnt;
    ElemType PC_1[56] = {
        56,48,40,32,24,16,8,
        0,57,49,41,33,25,17,
        9,1,58,50,42,34,26,
        18,10,2,59,51,43,35,
        62,54,46,38,30,22,14,
        6,61,53,45,37,29,21,
        13,5,60,52,44,36,28,
        20,12,4,27,19,11,3};

    for(cnt = 0; cnt < 56; cnt++)
    {
        tempbts[cnt] = key[PC_1[cnt]];
    }
}

//密钥置换2
void DES_PC2_Transform(ElemType key[56], ElemType tempbts[48])
{
    ElemType cnt;
    ElemType PC_2[48] = {
        13,16,10,23,0,4,2,27,
        14,5,20,9,22,18,11,3,
        25,7,15,6,26,19,12,1,
        40,51,30,36,46,54,29,39,
        50,44,32,47,43,48,38,55,
        33,52,45,41,49,35,28,31};

    for(cnt = 0; cnt < 48; cnt++)
    {
        tempbts[cnt] = key[PC_2[cnt]];
    }
}

//循环左移
void DES_ROL(ElemType *data_a,ElemType time)
{
    ElemType temp[56],i;

    //保存将要循环移动到左边的位
    for(i=0;i<time;i++)
    {
        temp[i] = *(data_a+i);
        temp[time+i] = *(data_a+28+i);
    }

    for(i=0;i<=28-time;i++)
    {
        *(data_a+i) =  *(data_a+time+i) ;
        *(data_a+28+i) =  *(data_a+28+time+i);
    }

    for(i=0;i<time;i++)
    {
        *(data_a+28-time+i) = temp[i];
        *(data_a+56-time+i) = temp[i+time];
    }
}

//循环右移
void DES_ROR(ElemType *data_a,ElemType time)
{
    ElemType temp[56],i;

    //保存将要循环移动到右边的位
    for(i=0;i<time;i++)
    {
        temp[time-1-i] = *(data_a+28-1-i);
        temp[time+1-i] = *(data_a+56-1-i);
    }

    for(i=0;i<=28-time;i++)
    {
        *(data_a+27-i) =  *(data_a+27-time-i);
        *(data_a+55-i) =  *(data_a+55-time-i);
    }

    for(i=0;i<time;i++)
    {
        *(data_a+i) = temp[i];
        *(data_a+28+i) = temp[i+2];
    }
}

//IP置换
void DES_IP_Transform(ElemType data_a[64])
{
    ElemType cnt;
    ElemType temp[64];
    ElemType IP_Table[64] = {
        57,49,41,33,25,17,9,1,
        59,51,43,35,27,19,11,3,
        61,53,45,37,29,21,13,5,
        63,55,47,39,31,23,15,7,
        56,48,40,32,24,16,8,0,
        58,50,42,34,26,18,10,2,
        60,52,44,36,28,20,12,4,
        62,54,46,38,30,22,14,6 };

    for(cnt = 0; cnt < 64; cnt++)
    {
        temp[cnt] = data_a[IP_Table[cnt]];
    }
    memcpy(data_a,temp,64);
}

//IP逆置换
void DES_IP_1_Transform(ElemType data_a[64])
{
    ElemType cnt;
    ElemType temp[64];
    ElemType IP_1_Table[64] = {
        39,7,47,15,55,23,63,31,
        38,6,46,14,54,22,62,30,
        37,5,45,13,53,21,61,29,
        36,4,44,12,52,20,60,28,
        35,3,43,11,51,19,59,27,
        34,2,42,10,50,18,58,26,
        33,1,41,9,49,17,57,25,
        32,0,40,8,48,16,56,24};

    for(cnt = 0; cnt < 64; cnt++)
    {
        temp[cnt] = data_a[IP_1_Table[cnt]];
    }
    memcpy(data_a,temp,64);
}

//扩展置换
void DES_E_Transform(ElemType data_a[48])
{
    ElemType cnt;
    ElemType temp[48];
    ElemType E_Table[48] = {
        31,0,1,2,3,4,
        3,4,5,6,7,8,
        7,8,9,10,11,12,
        11,12,13,14,15,16,
        15,16,17,18,19,20,
        19,20,21,22,23,24,
        23,24,25,26,27,28,
        27,28,29,30,31,0};

    for(cnt = 0; cnt < 48; cnt++)
    {
        temp[cnt] = data_a[E_Table[cnt]];
    }

    memcpy(data_a,temp,48);
}

//P置换
void DES_P_Transform(ElemType data_a[32])
{
    ElemType cnt;
    ElemType temp[32];
    ElemType P_Table[32] = {
        15,6,19,20,28,11,27,16,
        0,14,22,25,4,17,30,9,
        1,7,23,13,31,26,2,8,
        18,12,29,5,21,10,3,24};

    for(cnt = 0; cnt < 32; cnt++)
    {
        temp[cnt] = data_a[P_Table[cnt]];
    }
    memcpy(data_a,temp,32);
}

//异或
void DES_XOR(ElemType R[48], ElemType L[48] ,ElemType count)
{
    ElemType cnt;
    for(cnt = 0; cnt < count; cnt++)
    {
        R[cnt] ^= L[cnt];
    }
}

//S盒置换
void DES_SBOX(ElemType data_a[48])
{
    unsigned char cnt;
    unsigned char lines,row;
    unsigned char cur1,cur2;
    unsigned char output;

    //逆初始置换表IP^-1
    for(cnt = 0; cnt < 8; cnt++)
    {
        cur1 = cnt*6;
        cur2 = cnt<<2;

        //计算在S盒中的行与列
        lines = (data_a[cur1]<<1) + data_a[cur1+5];
        row = (data_a[cur1+1]<<3) + (data_a[cur1+2]<<2) + (data_a[cur1+3]<<1) + data_a[cur1+4];
        if(cnt<4)
        {
            output = S[cnt][lines][row];
        }
        else
        {
            output = S1[cnt-4][lines][row];
        }
        //化为2进制
        data_a[cur2] = (output&0X08)>>3;
        data_a[cur2+1] = (output&0X04)>>2;
        data_a[cur2+2] = (output&0X02)>>1;
        data_a[cur2+3] = output&0x01;
    }
}

//交换
void DES_Swap(ElemType left[32], ElemType right[32])
{
    ElemType temp[32];
    memcpy(temp,left,32);
    memcpy(left,right,32);
    memcpy(right,temp,32);
}

//加密单个分组
void DES_Encrypt(ElemType PlainTxt[8],ElemType *keyStr, ElemType CipherTxt[8])
{
    ElemType plainBits[64];
    ElemType copyRight[48];
    ElemType keyBlock[8];
    ElemType bKey[64];
    ElemType subKeys[48];
    ElemType temp[56];
    unsigned char cnt;
    unsigned char MOVE_TIMES[16] = {1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1};

    memcpy(keyBlock,keyStr,8);
    //将密钥转换为二进制流
    Char8ToBit64(keyBlock,bKey);

    DES_PC1_Transform(bKey,temp);//PC1置换

    Char8ToBit64(PlainTxt,plainBits);
    //初始置换（IP置换）
    DES_IP_Transform(plainBits);

    for(cnt = 0; cnt < 16; cnt++)
    {
        DES_ROL(temp,MOVE_TIMES[cnt]);		//左移
        DES_PC2_Transform(temp,subKeys);	//生成子密钥
        //DES_MakeSubKeys(subKeys,cnt);
        memcpy(copyRight,plainBits+32,32);
        //将右半部分进行扩展置换，从32位扩展到48位
        DES_E_Transform(copyRight);
        //将右半部分与子密钥进行异或操作

        DES_XOR(copyRight,subKeys,48);
        //异或结果进入S盒，输出32位结果

        DES_SBOX(copyRight);
        //P置换
        DES_P_Transform(copyRight);
        //将明文左半部分与右半部分进行异或
        DES_XOR(plainBits,copyRight,32);
        if(cnt != 15)
        {
            //最终完成左右部的交换
            DES_Swap(plainBits,plainBits+32);
        }
    }
    //逆初始置换（IP^1置换）
    DES_IP_1_Transform(plainBits);
    Bit64ToChar8(plainBits,CipherTxt);
}

//解密单个分组
void DES_Decrypt(ElemType PlainTxt[8],ElemType *keyStr,ElemType CipherTxt[8])
{
    ElemType cipherBits[64];
    ElemType copyRight[48];
    ElemType keyBlock[8];
    ElemType bKey[64];
    ElemType temp[56];
    ElemType subKeys[48];
    ElemType cnt;
    ElemType MOVE_TIMES[16] = {0,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1};
    memcpy(keyBlock,keyStr,8);
    //将密钥转换为二进制流
    Char8ToBit64(keyBlock,bKey);
    DES_PC1_Transform(bKey,temp);	 //PC1置换
    Char8ToBit64(PlainTxt,cipherBits);
    //初始置换（IP置换）
    DES_IP_Transform(cipherBits);

    //16轮迭代
    for(cnt = 0; cnt <16; cnt++)
    {
        if(cnt !=0)
        {
            DES_ROR(temp,MOVE_TIMES[cnt]);	 //右移
        }
        DES_PC2_Transform(temp,subKeys);	 //生成子密钥

        memcpy(copyRight,cipherBits+32,32);
        //将右半部分进行扩展置换，从32位扩展到48位
        DES_E_Transform(copyRight);
        //将右半部分与子密钥进行异或操作
        DES_XOR(copyRight,subKeys,48);
        //异或结果进入S盒，输出32位结果
        DES_SBOX(copyRight);
        //P置换
        DES_P_Transform(copyRight);
        //将明文左半部分与右半部分进行异或
        DES_XOR(cipherBits,copyRight,32);
        if(cnt != 15)
        {
            //最终完成左右部的交换
            DES_Swap(cipherBits,cipherBits+32);
        }
    }
    //逆初始置换（IP^1置换）
    DES_IP_1_Transform(cipherBits);
    Bit64ToChar8(cipherBits,CipherTxt);
}

/**********3DES加密*************/
void En3des(unsigned char m[8],unsigned char k[16],unsigned char e[8])
{
    unsigned char uszetmp[8];
    unsigned char uszmtmp[8];
    unsigned char uszkeytmp[8];

    //16字节key左半部分DES加密
    memcpy(uszkeytmp,k,8);
    DES_Encrypt(m,uszkeytmp,uszetmp);
    //16字节key右半部分DES解密
    memcpy(uszkeytmp,k+8,8);
    DES_Decrypt(uszetmp,uszkeytmp,uszmtmp);
    //16字节key左半部分DES加密
    memcpy(uszkeytmp,k,8);
    DES_Encrypt(uszmtmp,uszkeytmp,uszetmp);

    memcpy(e,uszetmp,8);
    return;
}

/**********3DES解密*******************/
void De3des(unsigned char e[8],unsigned char k[16],unsigned char m[8])
{
    unsigned char uszkeytmp[8];
    unsigned char uszetmp[8];
    unsigned char uszmtmp[8];

    //16字节左半部分DES解密
    memcpy(uszkeytmp,k,8);
    DES_Decrypt(e,uszkeytmp,uszmtmp);
    //16字节右半部分des加密
    memcpy(uszkeytmp,k+8,8);
    DES_Encrypt(uszmtmp,uszkeytmp,uszetmp);
    //16字节左半部分des解密
    memcpy(uszkeytmp,k,8);
    DES_Decrypt(uszetmp,uszkeytmp,uszmtmp);
    memcpy(m,uszmtmp,8);
    return;
}

/**
 * 3des encrypt an buffer(8 bytes align) with the key(16 bytes string).
 *
 * @param
 *    p_src[in]: the buffer to be encrypted.
 *    size[in]: size of 'p_src'
 *    key[in]: 3des key
 *    p_dst[out]: the result encrypted, size must not less then 'size'
 * @return
 *    0 success, -1 error
 */
int ss_3des_en(void * p_src, int size, unsigned char * key, void * p_dst)
{
    void * p_plain = 0;
    unsigned int i;
    char temp[8];

    if (p_src == NULL || p_dst == NULL || key == NULL)
    {
        printf("ss_3des_en input NULL pointer\n");
        return -1;
    }

    if (size % 8 != 0)
    {
        printf("ss_3des_en both size of src and dst buffer must be align 8, but size is: %d\n", size);
        return -1;
    }

    p_plain = p_src;

    // encrypt
    for(i = 0; i < size; i += 8)
    {
        memcpy(temp, p_plain + i, 8);
        En3des(temp, key, (unsigned char*)p_dst + i);
    }

    return 0;
}

/**
 * 3des decrypt an buffer(8 bytes align) with the key(16 bytes string).
 *
 * @param
 *    p_src[in]: the buffer to be decrypted.
 *    size[in]: size of 'p_src'
 *    key[in]: 3des key
 *    p_dst[out]: the result decrypted, size must not less then 'size'
 * @return
 *    0 success, -1 error
 */
int ss_3des_de(void * p_src, int size, unsigned char * key, void * p_dst)
{
    void * p_cipher = 0;
    unsigned int i;
    char temp[8];

    if (p_src == NULL || p_dst == NULL || key == NULL)
    {
        printf("ss_3des_de input NULL pointer\n");
        return -1;
    }

    if (size % 8 != 0)
    {
        printf("ss_3des_de both size of src and dst buffer must be align 8, but size is: %d\n", size);
        return -1;
    }

    p_cipher = p_src;

    // decrypt
    for(i = 0; i < size; i += 8)
    {
        memcpy(temp, p_cipher + i, 8);
        De3des(temp, key, (unsigned char*)p_dst + i);
    }

    return 0;
}

