/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2019/08/08
 *
 * History:
 *
 */

#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>

#include "osal_mutex.h"
#include "osal_cond.h"

/* Create a condition variable */
OSAL_cond * OSAL_CreateCond(void)
{
    OSAL_cond *cond;

    cond = (OSAL_cond *) OSAL_malloc(sizeof(OSAL_cond));
    if (cond) {
        if (pthread_cond_init(&cond->cond, NULL) != 0) {
            OSAL_SetError("pthread_cond_init() failed");
            OSAL_free(cond);
            cond = NULL;
        }
    }
    return (cond);
}

/* Destroy a condition variable */
void OSAL_DestroyCond(OSAL_cond * cond)
{
    if (cond) {
        pthread_cond_destroy(&cond->cond);
        OSAL_free(cond);
    }
}

/* Restart one of the threads that are waiting on the condition variable */
int OSAL_CondSignal(OSAL_cond * cond)
{
    int retval;

    if (!cond) {
        OSAL_SetError("Passed a NULL condition variable");
        return ErrorUnknown;
    }

    retval = 0;
    if (pthread_cond_signal(&cond->cond) != 0) {
        OSAL_SetError("pthread_cond_signal() failed");
        return ErrorUnknown;
    }
    return retval;
}

/* Restart all threads that are waiting on the condition variable */
int OSAL_CondBroadcast(OSAL_cond * cond)
{
    int retval;

    if (!cond) {
        OSAL_SetError("Passed a NULL condition variable");
        return ErrorUnknown;
    }

    retval = 0;
    if (pthread_cond_broadcast(&cond->cond) != 0) {
        OSAL_SetError("pthread_cond_broadcast() failed");
        return ErrorUnknown;
    }
    return retval;
}

int OSAL_CondWaitTimeout(OSAL_cond * cond, OSAL_mutex * mutex, unsigned int ms)
{
    int retval;
#ifndef HAVE_CLOCK_GETTIME
    struct timeval delta;
#endif
    struct timespec abstime;

    if (!cond) {
        OSAL_SetError("Passed a NULL condition variable");
        return ErrorUnknown;
    }

#ifdef HAVE_CLOCK_GETTIME
    clock_gettime(CLOCK_REALTIME, &abstime);

    abstime.tv_nsec += (ms % 1000) * 1000000;
    abstime.tv_sec += ms / 1000;
#else
    gettimeofday(&delta, NULL);

    abstime.tv_sec = delta.tv_sec + (ms / 1000);
    abstime.tv_nsec = (delta.tv_usec + (ms % 1000) * 1000) * 1000;
#endif
    if (abstime.tv_nsec > 1000000000) {
        abstime.tv_sec += 1;
        abstime.tv_nsec -= 1000000000;
    }

tryagain:
    retval = pthread_cond_timedwait(&cond->cond, &mutex->id, &abstime);
    switch (retval) {
    case EINTR:
        goto tryagain;
        /* break; -Wunreachable-code-break */
    case ETIMEDOUT:
        retval = ErrorUnknown;
        break;
    case 0:
        break;
    default:
        OSAL_SetError("pthread_cond_timedwait() failed");
        retval = ErrorUnknown;
    }
    return retval;
}

/* Wait on the condition variable, unlocking the provided mutex.
   The mutex must be locked before entering this function!
 */
int OSAL_CondWait(OSAL_cond * cond, OSAL_mutex * mutex)
{
    if (!cond) {
        OSAL_SetError("Passed a NULL condition variable");
        return ErrorUnknown;
    } else if (pthread_cond_wait(&cond->cond, &mutex->id) != 0) {
        OSAL_SetError("pthread_cond_wait() failed");
        return ErrorUnknown;
    }
    return 0;
}

/* vi: set ts=4 sw=4 expandtab: */
