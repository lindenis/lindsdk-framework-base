/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2020/10/13
 *
 * History:
 *
 */

#ifndef __RING_BUFFER_H__
#define __RING_BUFFER_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "osal_mutex.h"
#include "osal_types.h"

_handle_t ringbuf_create(int size);
void ringbuf_destroy(_handle_t h_ring);
int ringbuf_read(_handle_t h_ring, uint8_t * data, int size);
int ringbuf_write(_handle_t h_ring, uint8_t * data, int size);
int ringbuf_get_free_size(_handle_t h_ring);
int ringbuf_get_avail_size(_handle_t h_ring);
void ringbuf_dump(_handle_t h_ring);

#ifdef __cplusplus
}
#endif

#endif  // __RING_BUFFER_H__


