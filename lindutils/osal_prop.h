/*
 * Copyright (c) 2017-2022, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2022/01/04
 *
 * History:
 *
 */

#ifndef __LINDSDK_CONFIG_H_
#define __LINDSDK_CONFIG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <string.h>

#define SECTION_DEVICE      "device"
#define SECTION_LINDSDK     "lindsdk"
#define SECTION_DEBUG       "debug"

int         ldprop_init(const char * file_path);
void        ldprop_deinit();

const char *ldprop_get_string(const char * sec, const char * key, const char * def);
int         ldprop_get_int(const char * sec, const char * key, int notfound);
long int    ldprop_get_longint(const char * sec, const char * key, long int notfound);
int         ldprop_get_boolean(const char * sec, const char * key, int notfound);
double      ldprop_get_double(const char * sec, const char * key, double notfound);

const char *ldprop_get(const char * key);
int         ldprop_set(const char * key, const char * val);
int         ldprop_save(const char * file_path);
int         ldprop_sync();
void        ldprop_dump();

#ifdef __cplusplus
}
#endif

#endif  // __LINDSDK_CONFIG_H_

