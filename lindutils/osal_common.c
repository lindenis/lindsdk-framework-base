/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2019/08/08
 *
 * History:
 *
 */

#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#ifdef WIN32
#include <winsock2.h>
#include <ws2tcpip.h>
#include <Iphlpapi.h>
#pragma comment(lib, "ws2_32.lib")
#pragma comment(lib,"Iphlpapi.lib")
#else
#include <netdb.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#ifndef MELIS_OS
#include <sys/ioctl.h>
#else
#include "clock_time.h"
#endif
#endif

#include "osal_common.h"
#include "osal_log.h"

#define MAC_SIZE 18

long long getNowUs(void)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return (long long)tv.tv_sec * 1000000ll + tv.tv_usec;
}

long long get_tick_us()
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ((long long)ts.tv_sec * 1000000ll + ts.tv_nsec / 1000);
}

#ifdef WIN32
long long get_tick_ms()
{
    return GetTickCount();
}
#else
long long get_tick_ms()
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return (ts.tv_sec * 1000 + ts.tv_nsec / 1000000);
}
#endif

char * getError()
{
    return strerror(errno);
}

void str2lower(const char * src, char * dst)
{
    int i = 0;
    while(src[i])
    {
        dst[i] = tolower(src[i]);
        i++;
    }
    dst[i] = 0;
}

void str2upper(const char * src, char * dst)
{
    int i = 0;
    while(src[i])
    {
        dst[i] = toupper(src[i]);
        i++;
    }
    dst[i] = 0;
}

int wait_input_char(signed char * ch, long long time_out_us)
{
    fd_set rfds;
    struct timeval tv;
    int ret;
    FD_ZERO(&rfds);
    FD_SET(0, &rfds);

    /* wait until timeout. */
    tv.tv_sec = 0;
    tv.tv_usec = time_out_us;
    ret = select(1, &rfds, NULL, NULL, &tv);
    if (ret == -1)
    {
        perror("select()");
    }
    else if (ret == 0)
    {
        // Data is available now
    }
    else
    {
        // now can read
        *ch = getchar();
    }
    return ret;
}

int read_file(char * filepath, void * buf, int bufsize)
{
    int ret = 0;
    FILE * fd = 0;
    int readsize = 0;
    int filesize = 0;

    fd = fopen(filepath, "r");
    if (NULL == fd)
    {
        printf("open file %s failed", filepath);
        return -1;
    }

    fseek(fd, 0L, SEEK_END);
    filesize = ftell(fd);

    readsize = (filesize < bufsize) ? filesize : bufsize;

    fseek(fd, 0L, SEEK_SET);
    // to fixup the case ret != readsize
    ret = fread(buf, 1, readsize, fd);

    if (fd > 0)
        fclose(fd);

    return ret;
}

int save_file(char * filepath, void * buf, int bufsize)
{
    FILE * fd = 0;
    fd = fopen(filepath, "wb+");
    if (NULL == fd)
    {
        loge("open file %s failed", filepath);
        return -1;
    }
    fwrite(buf, bufsize, 1, fd);
    fclose(fd);
    fd = 0;

    return 0;
}

int file_size(char * filepath)
{
    FILE * fd = 0;
    int filesize = 0;

    fd = fopen(filepath, "r");
    if (NULL == fd)
    {
        printf("open file %s failed", filepath);
        return -1;
    }

    fseek(fd, 0L, SEEK_END);
    filesize = ftell(fd);
    fclose(fd);

    return filesize;
}

void system_cmd(const char * command, char * result)
{
    FILE *fpRead;
    fpRead = popen(command, "r");
    char buf[1024] = {0};
    memset(buf,'\0',sizeof(buf));
    while(fgets(buf,1024-1,fpRead) != NULL)
    {
        if (buf[strlen(buf) - 1] == '\n') {
            buf[strlen(buf) - 1] = '\0';        //ȥ�����з�
        }
        strcpy(result, buf);
    }
    if(fpRead != NULL)
        pclose(fpRead);
}

int system_cmd_find_key(const char * command, char * key)
{
    FILE *fpRead;
    int find = 0;
    fpRead = popen(command, "r");
    char buf[1024] = {0};
    memset(buf,'\0',sizeof(buf));
    logc("cmd: %s, key: %s", command, key);
    while(fgets(buf,1024-1,fpRead) != NULL)
    {
        if (buf[strlen(buf) - 1] == '\n') {
            buf[strlen(buf) - 1] = '\0';        //ȥ�����з�
        }
        logc("--- %s", buf);
        if (strstr(buf, key) != NULL)
        {
            find = 1;
        }
    }
    if(fpRead != NULL)
        pclose(fpRead);

    return (find == 1) ? 0 : -1;
}

int system_cmd_get_keyvalue(const char * command, char * key, char * value)
{
    char *pval = NULL;
    char *seps = "=: \\r\\n";
    int find = 0;
    FILE *fpRead;
    fpRead = popen(command, "r");
    char buf[1024] = {0};
    memset(buf,'\0',sizeof(buf));

    while(fgets(buf,1024-1,fpRead) != NULL)
    {
        if (buf[strlen(buf) - 1] == '\n') {
            buf[strlen(buf) - 1] = '\0';
        }
        if (!strncmp(key, buf, strlen(key)))
        {
            pval = buf;
            pval = strtok(buf, seps);
            while (pval != NULL)
            {
                strcpy(value, pval);
                pval = strtok(NULL, seps);
            }
            find = 1;
            break;
        }
    }

    if(fpRead != NULL)
    {
        pclose(fpRead);
    }

    return (find == 1) ? 0 : -1;
}

int get_hardware(char * hardware)
{
    char cmd[64];

    sprintf(cmd, "cat /proc/cpuinfo");
    return system_cmd_get_keyvalue(cmd, "Hardware", hardware);
}

void dump_hex(unsigned char * data, int size, int align)
{
    int i = 0;
    if (data == NULL
        || size == 0
        || align == 0)
    {
        printf("input parameters error\n");
        return;
    }

    for (i = 0; i < size; i++)
    {
        if (i != 0 && i % align == 0)
            printf("\n");
        printf("%02x ", data[i]);
    }
    printf("\n");
}

void get_datetime(char * buf, int max_size, int weekday)
{
    time_t timep;
    struct tm *p;
    time(&timep);
    p = localtime(&timep);

    char cDate[64];
    memset( cDate, 0, sizeof(cDate));
    snprintf(cDate, sizeof(cDate)-1,"%d-%02d-%02d",(1900 + p->tm_year), (1 + p->tm_mon), p->tm_mday);

    if (weekday) {
        char *wday[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        snprintf(buf, max_size, "%s %s %02d:%02d:%02d", cDate, wday[p->tm_wday], p->tm_hour, p->tm_min, p->tm_sec);
    } else {
        snprintf(buf, max_size, "%s %02d:%02d:%02d", cDate, p->tm_hour, p->tm_min, p->tm_sec);
    }
}

#if (defined WIN32)
void dump_net_info()
{
    PIP_ADAPTER_INFO pIpAdapter = (PIP_ADAPTER_INFO)malloc(sizeof(PIP_ADAPTER_INFO));
    unsigned long stSize = sizeof(IP_ADAPTER_INFO);
    int nRel = GetAdaptersInfo(pIpAdapter, &stSize);
    int netCardNum = 0;
    char strType[16];
    char strMac[32];
    if (ERROR_BUFFER_OVERFLOW == nRel)
    {
        free(pIpAdapter);
        pIpAdapter = (PIP_ADAPTER_INFO)malloc(stSize);
        nRel=GetAdaptersInfo(pIpAdapter,&stSize);
    }
    PIP_ADAPTER_INFO pIpAdapterInfo = pIpAdapter;
    if (ERROR_SUCCESS == nRel)
    {
        while (pIpAdapterInfo)
        {
            strcpy(strType, "");
            strcpy(strMac, "");
            switch(pIpAdapterInfo->Type)
            {
            case MIB_IF_TYPE_OTHER:
                strcpy(strType, "OTHER");
                break;
            case MIB_IF_TYPE_ETHERNET:
                strcpy(strType, "ETHERNET");
                break;
            case MIB_IF_TYPE_TOKENRING:
                strcpy(strType, "TOKENRING");
                break;
            case MIB_IF_TYPE_FDDI:
                strcpy(strType, "FDDI");
                break;
            case MIB_IF_TYPE_PPP:
                strcpy(strType, "PPP");
                break;
            case MIB_IF_TYPE_LOOPBACK:
                strcpy(strType, "LOOPBACK");
                break;
            case MIB_IF_TYPE_SLIP:
                strcpy(strType, "SLIP");
                break;
            case IF_TYPE_IEEE80211:
                strcpy(strType, "WIRELESS");
                break;
            default:
                strcpy(strType, "UNKNOWN");
                logw("type: %d", pIpAdapterInfo->Type);
                break;
            }

            for (DWORD i = 0; i < pIpAdapterInfo->AddressLength; i++)
            {
                int len = strlen(strMac);
                if (i < pIpAdapterInfo->AddressLength-1) {
                    sprintf(strMac + len, "%02X:", pIpAdapterInfo->Address[i]);
                } else {
                    sprintf(strMac + len, "%02X", pIpAdapterInfo->Address[i]);
                }
            }
            logd("Number        : %d", ++netCardNum);
            logd("NET card name : %s", pIpAdapterInfo->AdapterName);
            logd("Description   : %s", pIpAdapterInfo->Description);
            logd("NET card type : %s", strType);
            logd("NET MAC       : %s", strMac);
            IP_ADDR_STRING *pIpAddrString =&(pIpAdapterInfo->IpAddressList);
            do
            {
                logd("IP Addr       : %s", pIpAddrString->IpAddress.String);
                logd("IP Mask       : %s", pIpAddrString->IpMask.String);
                logd("IP Gate       : %s", pIpAdapterInfo->GatewayList.IpAddress.String);
                pIpAddrString=pIpAddrString->Next;
            } while (pIpAddrString);
            pIpAdapterInfo = pIpAdapterInfo->Next;
        }
    }

    if (pIpAdapter)
    {
        free(pIpAdapter);
    }
}

int get_net_info(const char * net_inf, char * mac, char * ip, char * mask, char * bcast, char * gate)
{
    PIP_ADAPTER_INFO pIpAdapter = (PIP_ADAPTER_INFO)malloc(sizeof(PIP_ADAPTER_INFO));
    unsigned long stSize = sizeof(IP_ADAPTER_INFO);
    int nRel = GetAdaptersInfo(pIpAdapter, &stSize);
    int netCardNum = 0;
    char strMac[32];
    int iRet = -1;
    if (ERROR_BUFFER_OVERFLOW == nRel)
    {
        free(pIpAdapter);
        pIpAdapter = (PIP_ADAPTER_INFO)malloc(stSize);
        nRel=GetAdaptersInfo(pIpAdapter,&stSize);
    }
    PIP_ADAPTER_INFO pIpAdapterInfo = pIpAdapter;
    if (ERROR_SUCCESS == nRel)
    {
        while (pIpAdapterInfo)
        {
            if (!strcmp(pIpAdapterInfo->IpAddressList.IpAddress.String, "0.0.0.0")) {
                pIpAdapterInfo = pIpAdapterInfo->Next;
                continue;
            }
            str2lower(pIpAdapterInfo->Description, pIpAdapterInfo->Description);
            if ((!strcmp(net_inf, "eth0")
                  && (pIpAdapterInfo->Type == MIB_IF_TYPE_ETHERNET)
                  && !strstr(pIpAdapterInfo->Description, "bluetooth"))
                || (!strcmp(net_inf, "wlan0")
                  && (pIpAdapterInfo->Type == IF_TYPE_IEEE80211)
                  && !strstr(pIpAdapterInfo->Description, "virtual")))
            {
                logd("find net card : %s", net_inf);

                strcpy(strMac, "");
                for (DWORD i = 0; i < pIpAdapterInfo->AddressLength; i++)
                {
                    int len = strlen(strMac);
                    if (i < pIpAdapterInfo->AddressLength-1) {
                        sprintf(strMac + len, "%02X:", pIpAdapterInfo->Address[i]);
                    } else {
                        sprintf(strMac + len, "%02X", pIpAdapterInfo->Address[i]);
                    }
                }

                if (ip) {
                    strcpy(ip, pIpAdapterInfo->IpAddressList.IpAddress.String);
                }
                if (mask) {
                    strcpy(mask, pIpAdapterInfo->IpAddressList.IpMask.String);
                }
                if (gate) {
                    strcpy(gate, pIpAdapterInfo->GatewayList.IpAddress.String);
                }
                if (mac) {
                    strcpy(mac, strMac);
                }
                if (bcast) {
                    struct sockaddr_in ip_addr;
                    ip_addr.sin_addr.s_addr = inet_addr(ip);
                    unsigned char * ip_data = (unsigned char *)&(ip_addr.sin_addr.s_addr);

                    struct sockaddr_in mask_addr;
                    mask_addr.sin_addr.s_addr = inet_addr(mask);
                    unsigned char * mask_data = (unsigned char *)&(mask_addr.sin_addr.s_addr);

                    unsigned char bcast_data[4];
                    for (int i = 0; i < 4; i++) {
                        bcast_data[i] = ip_data[i] | ~mask_data[i];
                    }
                    sprintf(bcast, "%d.%d.%d.%d", bcast_data[0], bcast_data[1], bcast_data[2], bcast_data[3]);
                }

                iRet = 0;
                break;
            }
            else
            {
                pIpAdapterInfo = pIpAdapterInfo->Next;
                continue;
            }
        }
    }

    if (pIpAdapter)
    {
        free(pIpAdapter);
    }

    return iRet;
}

#elif (defined MELIS_OS)
#include "tcpip_adapter.h"
void dump_net_info()
{
    int ret = 0;
    char mac[32];
    char ip[32];
    char mask[32];
    char bcast[32];
    char gate[32];
    ret = get_net_info(NULL, mac, ip, mask, bcast, gate);
    if (ret == 0)
    {
        logd("NET MAC       : %s", mac);
        logd("IP Addr       : %s", ip);
        logd("IP Mask       : %s", mask);
        logd("IP Bcast      : %s", bcast);
        logd("IP Gate       : %s", gate);
    }
}

int get_net_info(const char * net_inf, char * mac, char * ip, char * mask, char * bcast, char * gate)
{
    struct netif * p_net_info = get_netif(MODE_STA);
    if (p_net_info == NULL) {
        return -1;
    }
    if (mac) {
        sprintf(mac, "%02X:%02X:%02X:%02X:%02X:%02X",
            p_net_info->hwaddr[0], p_net_info->hwaddr[1],
            p_net_info->hwaddr[2], p_net_info->hwaddr[3],
            p_net_info->hwaddr[4], p_net_info->hwaddr[5]);
    }
    if (ip) {
        sprintf(ip, "%d.%d.%d.%d",
            ip4_addr1_16(netif_ip4_addr(p_net_info)),
            ip4_addr2_16(netif_ip4_addr(p_net_info)),
            ip4_addr3_16(netif_ip4_addr(p_net_info)),
            ip4_addr4_16(netif_ip4_addr(p_net_info)));
    }
    if (gate) {
        sprintf(gate, "%d.%d.%d.%d",
            ip4_addr1_16(netif_ip4_gw(p_net_info)),
            ip4_addr2_16(netif_ip4_gw(p_net_info)),
            ip4_addr3_16(netif_ip4_gw(p_net_info)),
            ip4_addr4_16(netif_ip4_gw(p_net_info)));
    }
    if (mask) {
        sprintf(mask, "%d.%d.%d.%d",
            ip4_addr1_16(netif_ip4_netmask(p_net_info)),
            ip4_addr2_16(netif_ip4_netmask(p_net_info)),
            ip4_addr3_16(netif_ip4_netmask(p_net_info)),
            ip4_addr4_16(netif_ip4_netmask(p_net_info)));
    }
    if (bcast) {
        /*
         * (mask & ip) | ~(mask)
         */
        uint32_t * p_mask = (uint32_t *)&p_net_info->netmask;
        uint32_t * p_ip = (uint32_t *)&p_net_info->ip_addr;
        uint32_t bcast_ip = (*p_mask & *p_ip | (~*p_mask));
        uint8_t * p_bcast_ip = (uint8_t *)&bcast_ip;
        sprintf(bcast, "%d.%d.%d.%d", p_bcast_ip[0], p_bcast_ip[1], p_bcast_ip[2], p_bcast_ip[3]);
    }

    return 0;
}

#else

void dump_net_info()
{
    int ret = 0;
    char mac[32];
    char ip[32];
    char mask[32];
    char bcast[32];
    char gate[32];
    char * net_inf[8] = {"hg0", "eth0", "wlan0", NULL};

    int index = 0;
    char * p_if = net_inf[0];
    while(p_if != NULL)
    {
        ret = get_net_info(p_if, mac, ip, mask, bcast, gate);
        if (ret == 0)
        {
            logd("NET card name : %s", p_if);
            logd("NET MAC       : %s", mac);
            logd("IP Addr       : %s", ip);
            logd("IP Mask       : %s", mask);
            logd("IP Bcast      : %s", bcast);
            logd("IP Gate       : %s", gate);
        }
        mac[0]      = 0;
        ip[0]       = 0;
        mask[0]     = 0;
        bcast[0]    = 0;
        gate[0]     = 0;
        p_if = net_inf[++index];
    }
}

int get_net_info(const char * net_inf, char * mac, char * ip, char * mask, char * bcast, char * gate)
{
    int sock;
    struct sockaddr_in sin;
    struct ifreq ifr;

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (-1 == sock)
    {
        loge("socket error: %s", strerror(errno));
        return -1;
    }

    strncpy(ifr.ifr_name, net_inf, IFNAMSIZ);
    ifr.ifr_name[IFNAMSIZ - 1] = 0;

    if (ip)
    {
        if (ioctl(sock, SIOCGIFADDR, &ifr) < 0)
        {
            logw("ioctl error: %s, %s", strerror(errno), net_inf);
            close(sock);
            return -1;
        }

        memcpy(&sin, &ifr.ifr_addr, sizeof(sin));
        sprintf(ip, "%s", inet_ntoa(sin.sin_addr));
    }

    if (mask)
    {
        if (ioctl(sock, SIOCGIFNETMASK, &ifr) < 0)
        {
            logw("ioctl error: %s, %s", strerror(errno), net_inf);
            close(sock);
            return -1;
        }

        memcpy(&sin, &ifr.ifr_addr, sizeof(sin));
        sprintf(mask, "%s", inet_ntoa(sin.sin_addr));
    }

    if (bcast)
    {
        if (ioctl(sock, SIOCGIFBRDADDR, &ifr) < 0)
        {
            logw("ioctl error: %s, %s", strerror(errno), net_inf);
            close(sock);
            return -1;
        }

        memcpy(&sin, &ifr.ifr_addr, sizeof(sin));
        sprintf(bcast, "%s", inet_ntoa(sin.sin_addr));
    }

    if (mac)
    {
        if(ioctl(sock, SIOCGIFHWADDR, &ifr) < 0)
        {
            logw("get %s mac address failed", net_inf);
            close(sock);
            return -1;
        }

        snprintf(mac, MAC_SIZE, "%02X:%02X:%02X:%02X:%02X:%02X",
            (unsigned char)ifr.ifr_hwaddr.sa_data[0],
            (unsigned char)ifr.ifr_hwaddr.sa_data[1],
            (unsigned char)ifr.ifr_hwaddr.sa_data[2],
            (unsigned char)ifr.ifr_hwaddr.sa_data[3],
            (unsigned char)ifr.ifr_hwaddr.sa_data[4],
            (unsigned char)ifr.ifr_hwaddr.sa_data[5]);
    }

    if (gate)
    {
        // do no support
    }

    close(sock);
    return 0;
}
#endif  // WIN32

int get_local_ip(const char * net_inf, char * ip)
{
    return get_net_info(net_inf, NULL, ip, NULL, NULL, NULL);
}

int get_broad_ip(const char * net_inf, char * ip)
{
    return get_net_info(net_inf, NULL, NULL, NULL, ip, NULL);
}

int get_net_mask(const char * net_inf, char * ip)
{
    return get_net_info(net_inf, NULL, NULL, ip, NULL, NULL);
}

int get_local_mac(const char * net_inf, char * mac)
{
    return get_net_info(net_inf, mac, NULL, NULL, NULL, NULL);
}

int get_default_ip(char * ip)
{
    if (
#ifndef WIN32
        get_local_ip("hg0", ip) == 0 ||
#endif
        (get_local_ip("eth0", ip) == 0 && strcmp(ip, "0.0.0.0"))
        || (get_local_ip("wlan0", ip) == 0 && strcmp(ip, "0.0.0.0"))
        || (get_local_ip("lo", ip) == 0 && strcmp(ip, "0.0.0.0")))
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

int get_default_broad_ip(char * ip)
{
    if (
#ifndef WIN32
        get_broad_ip("hg0", ip) == 0 ||
#endif
        get_broad_ip("eth0", ip) == 0
        || get_broad_ip("wlan0", ip) == 0
        || get_broad_ip("lo", ip) == 0)
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

int get_default_net_mask(char * ip)
{
    if (
#ifndef WIN32
        get_net_mask("hg0", ip) == 0 ||
#endif
        get_net_mask("eth0", ip) == 0
        || get_net_mask("wlan0", ip) == 0
        || get_net_mask("lo", ip) == 0)
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

int get_default_mac(char * mac)
{
    if (
#ifndef WIN32
        get_local_mac("hg0", mac) == 0 ||
#endif
        get_local_mac("eth0", mac) == 0
        || get_local_mac("wlan0", mac) == 0
        || get_local_mac("lo", mac) == 0)
    {
        return 0;
    }
    else
    {
        return -1;
    }
}

