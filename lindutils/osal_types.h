/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2019/08/08
 *
 * History:
 *
 */

#ifndef __OSAL_TYPES_H__
#define __OSAL_TYPES_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef SUCCESS
#define SUCCESS 0
#endif
#ifndef FAILED
#define FAILED  -1
#endif

#include <stdint.h>

#ifdef _WIN32
#include <windows.h>
#endif

#ifndef _SYS__STDINT_H
#define _SYS__STDINT_H

#ifndef _STDINT_H
#ifndef __BIT_TYPES_DEFINED__
#define __BIT_TYPES_DEFINED__

#ifndef __DEFINED_int8_t
typedef signed char         int8_t;
#define __DEFINED_int8_t
#endif

#ifndef __DEFINED_int16_t
typedef signed short        int16_t;
#define __DEFINED_int16_t
#endif

#ifndef __DEFINED_int32_t
typedef signed int          int32_t;
#define __DEFINED_int32_t
#endif

#ifndef __DEFINED_int64_t
typedef signed long long    int64_t;
#define __DEFINED_int64_t
#endif

#endif /* !(__BIT_TYPES_DEFINED__) */

#ifndef _BITS_STDINT_UINTN_H
typedef unsigned char       uint8_t;
typedef unsigned short      uint16_t;
typedef unsigned int        uint32_t;
typedef unsigned long long	uint64_t;
#endif // _BITS_STDINT_UINTN_H

#endif // _STDINT_H

#endif // _SYS__STDINT_H, melis

typedef struct _rect_t
{
    int left;
    int top;
    int width;
    int height;
} rect_t;

/*
 * thread
 */
#ifdef win_x86_64
    typedef LPVOID          _thread_param_t;
    typedef DWORD WINAPI    _thread_ret_t;
#else
    typedef void *          _thread_param_t;
    typedef void *          _thread_ret_t;
#endif

/*
 * handle
 */
typedef long                _handle_t;

#ifdef WIN32
    #define _sleep_ms(x)    Sleep(x)
#else
    #define _sleep_ms(x)    usleep(x*1000);
#endif

typedef int (* comm_msg_cb)(void * user, int msg, void * ext1, void * ext2);

typedef struct _net_addr_t
{
    char            ip[32];
    uint16_t        port;
} net_addr_t;

#ifdef __cplusplus
}
#endif

#endif  // __OSAL_TYPES_H__

