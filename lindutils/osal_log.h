/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2019/08/08
 *
 * History:
 *
 */

#ifndef __OSAL_LOG_H_
#define __OSAL_LOG_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <string.h>

#ifndef LOG_TAG
#define LOG_TAG             "ld_dbg"
#endif

#if ((defined x86_64) || (defined x86_32))
    #define LOG_TYPE_PRINTF
#elif (defined LINUX_OS)
    #define LOG_TYPE_ELOG
#elif (defined WIN32)
    #define LOG_TYPE_WIN32
#elif (defined ANDROID_OS)
    #define LOG_TYPE_ANDROID
#else
    #define LOG_TYPE_PRINTF
#endif

#ifdef WIN32
#define __FILENAME__(x) strrchr(x,'\\')?strrchr(x,'\\')+1:x
#else
#define __FILENAME__(x) strrchr(x,'/')?strrchr(x,'/')+1:x
#endif

typedef enum {
    LOG_LVL_FATAL       = 0,
    LOG_LVL_ERROR       = 1,
    LOG_LVL_WARN        = 2,
    LOG_LVL_INFO        = 3,
    LOG_LVL_DEBUG       = 4,
    LOG_LVL_VERBOSE     = 5,
    LOG_LVL_SILENT      = 6,
} lind_log_level_e;

void lind_log_output(unsigned char level, const char *tag, const char *file, const char *func,
        const long line, const char *format, ...);

#define logf(...) do { \
    lind_log_output(LOG_LVL_FATAL  , LOG_TAG, __FILENAME__(__FILE__), __FUNCTION__, __LINE__, __VA_ARGS__);}while(0)
#define loge(...) do { \
    lind_log_output(LOG_LVL_ERROR  , LOG_TAG, __FILENAME__(__FILE__), __FUNCTION__, __LINE__, __VA_ARGS__);}while(0)
#define logw(...) do { \
    lind_log_output(LOG_LVL_WARN   , LOG_TAG, __FILENAME__(__FILE__), __FUNCTION__, __LINE__, __VA_ARGS__);}while(0)
#define logi(...) do { \
    lind_log_output(LOG_LVL_INFO   , LOG_TAG, __FILENAME__(__FILE__), __FUNCTION__, __LINE__, __VA_ARGS__);}while(0)
#define logd(...) do { \
    lind_log_output(LOG_LVL_DEBUG  , LOG_TAG, __FILENAME__(__FILE__), __FUNCTION__, __LINE__, __VA_ARGS__);}while(0)
#define logv(...) do { \
    lind_log_output(LOG_LVL_VERBOSE, LOG_TAG, __FILENAME__(__FILE__), __FUNCTION__, __LINE__, __VA_ARGS__);}while(0)

#define logc(...) do { \
    lind_log_output(LOG_LVL_INFO   , LOG_TAG, __FILENAME__(__FILE__), __FUNCTION__, __LINE__, __VA_ARGS__);}while(0)

#define F_LOG printf("%s, line: %d\n", __func__, __LINE__);

#ifdef __cplusplus
}
#endif

#endif  // __OSAL_LOG_H_

