/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2019/08/08
 *
 * History:
 *
 */

#ifndef __OSAL_MUTEX__
#define __OSAL_MUTEX__

#ifdef __cplusplus
extern "C" {
#endif

#include <pthread.h>

typedef long HANDLETYPE;

#define ErrorNone       0
#define ErrorUnknown   -1

typedef struct OSAL_mutex_t
{
    pthread_mutex_t id;
} OSAL_mutex;

OSAL_mutex * OSAL_CreateMutex();
int OSAL_DestroyMutex(OSAL_mutex * mutex);
int OSAL_LockMutex(OSAL_mutex * mutex);
int OSAL_TryLockMutex(OSAL_mutex * mutex);
int OSAL_UnlockMutex(OSAL_mutex * mutex);

int OSAL_MutexCreate(HANDLETYPE *mutexHandle);
int OSAL_MutexTerminate(HANDLETYPE mutexHandle);
int OSAL_MutexLock(HANDLETYPE mutexHandle);
int OSAL_MutexTryLock(HANDLETYPE mutexHandle);
int OSAL_MutexUnlock(HANDLETYPE mutexHandle);

#define lock_mutex          pthread_mutex_t
#define init_lock(X)        pthread_mutex_init(X,NULL)
#define do_lock(X)          pthread_mutex_lock(X)
#define do_unlock(X)        pthread_mutex_unlock(X)
#define destroy_lock(X)     pthread_mutex_destroy(X)

#ifdef __cplusplus
}
#endif


#endif

