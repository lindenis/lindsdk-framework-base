/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2019/08/08
 *
 * History:
 *
 */

#ifndef __OSAL_QUEUE__
#define __OSAL_QUEUE__

#ifdef __cplusplus
extern "C" {
#endif

#include "osal_types.h"
#include "osal_mutex.h"

#define QUEUE_BLOCK         1
#define QUEUE_UNBLOCK       0

typedef struct _OSAL_QElem
{
    void               *data;
    struct _OSAL_QElem *qNext;
} OSAL_QElem;

typedef struct _OSAL_QUEUE
{
    OSAL_QElem     *first;
    OSAL_QElem     *last;
#ifndef MELIS_OS
    int            numElem;
#endif
    HANDLETYPE     qMutex;
    int            maxElem;
} OSAL_QUEUE;

int           OSAL_QueueCreate(OSAL_QUEUE *queueHandle, int maxQueueElem);
int           OSAL_QueueTerminate(OSAL_QUEUE *queueHandle);
int           OSAL_Queue(OSAL_QUEUE *queueHandle, void *data);
void         *OSAL_Dequeue(OSAL_QUEUE *queueHandle);
int           OSAL_GetElemNum(OSAL_QUEUE *queueHandle);
int           OSAL_SetElemNum(OSAL_QUEUE *queueHandle, int ElemNum);
int			  OSAL_QueueSetElem(OSAL_QUEUE *queueHandle, void *data);

typedef void (*f_free_obj)(void * obj);

_handle_t   osal_queue_create(int max, char * queue_name);
void        osal_queue_destroy(_handle_t h_queue);
int         osal_enqueue(_handle_t h_queue, void * obj, int block);
int         osal_dequeue(_handle_t h_queue, void ** obj, int block);
int         osal_dequeue_timeout(_handle_t h_queue, void ** obj, int timeout_ms);
int         osal_queue_get_nb(_handle_t h_queue);
void        osal_queue_flush(_handle_t h_queue, int free_obj);
void        osal_queue_flush2(_handle_t h_queue, f_free_obj free_obj);
void        osal_queue_abort(_handle_t h_queue);
int         osal_queue_head(_handle_t h_queue, void ** obj);

#ifdef __cplusplus
}
#endif

#endif	// __OSAL_QUEUE__
