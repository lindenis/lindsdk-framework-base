/*
 * Copyright (c) 2017-2019, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2019/08/08
 *
 * History:
 *
 */

#ifndef __LD_UUID_H__
#define __LD_UUID_H__

#ifdef __cplusplus
extern "C" {
#endif

#define LD_UUID_VERSION "1.0.0"
#define LD_UUID_LEN 37

int ld_uuid_generate(char * uuid);

#ifdef __cplusplus
}
#endif

#endif // __LD_UUID_H__


