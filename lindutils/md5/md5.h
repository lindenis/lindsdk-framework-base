#ifndef _OSAL_MD5_H_
#define _OSAL_MD5_H_

#ifdef __cplusplus
extern "C" {
#endif

void md5(const uint8_t * msg, size_t len, uint8_t *digest);

#ifdef __cplusplus
}
#endif

#endif //_OSAL_MD5_H_

