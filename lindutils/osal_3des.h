#ifndef _OSAL_3DES_H_
#define _OSAL_3DES_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <string.h>

#define	uchar   unsigned  char
#define	uint    unsigned  int
#define	TRUE    1
#define	FALSE   0

typedef	unsigned int        DWORD; //4
typedef	int                 BOOL;
typedef unsigned char       ElemType;

char Char8ToBit64(ElemType *ch,ElemType *bit_a);  		  			//8字节转换成64位
void Bit64ToChar8(ElemType bit_a[64],ElemType ch[8]);  				//64位转换成8字节
void DES_PC1_Transform(ElemType key[64], ElemType tempbts[56]);  	// 实现置换选择1
void DES_PC2_Transform(ElemType key[56], ElemType tempbts[48]);  	// 实现置换选择2
void DES_ROL(ElemType *data_a, ElemType time);  					//左移函数用于子密钥
void DES_ROR(ElemType *data_a, ElemType time);  					//右移函数用于子密钥
void DES_IP_Transform(ElemType data_a[64]);  						// 实现IP置换选择
void DES_IP_1_Transform(ElemType data_a[64]);  						// 实现IP_1置换选择
void DES_E_Transform(ElemType data_a[48]);  						// 实现E盒扩展
void DES_P_Transform(ElemType data_a[32]);  						//实现p盒扩置换
void DES_SBOX(ElemType data_a[48]);  								//S盒函数实现
void DES_XOR(ElemType R[48], ElemType L[48],ElemType count);  		// 实现异或功能
void DES_Swap(ElemType left[32],ElemType right[32]);  				//左右两部分交换
void DES_Encrypt(ElemType *plainFile, ElemType *keyStr,ElemType *cipherFile);  	//加密数据
void DES_Decrypt(ElemType *cipherFile,ElemType *keyStr,ElemType *plainFile); 	//解密数据

/**
 * 3des encrypt an buffer(8 bytes align) with the key(16 bytes string).
 *
 * @param
 *    p_src[in]: the buffer to be encrypted.
 *    size[in]: size of 'p_src'
 *    key[in]: 3des key
 *    p_dst[out]: the result encrypted, size must not less then 'size'
 * @return
 *    0 success, -1 error
 */
int ss_3des_en(void * p_src, int size, unsigned char * key, void * p_dst);

/**
 * 3des decrypt an buffer(8 bytes align) with the key(16 bytes string).
 *
 * @param
 *    p_src[in]: the buffer to be decrypted.
 *    size[in]: size of 'p_src'
 *    key[in]: 3des key
 *    p_dst[out]: the result decrypted, size must not less then 'size'
 * @return
 *    0 success, -1 error
 */
int ss_3des_de(void * p_src, int size, unsigned char * key, void * p_dst);

#ifdef __cplusplus
}
#endif

#endif //_OSAL_3DES_H_

