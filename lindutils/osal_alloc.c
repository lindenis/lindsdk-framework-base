/*
 * Copyright (c) 2017-2021, Lindenis Tech. Ltd.
 * All rights reserved.
 *
 * File:
 *
 * Description:
 *
 * Author:
 *      xiaoshujun@lindeni.com
 *
 * Create Date:
 *      2021/12/13
 *
 * History:
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define _PRINT_ALLOC_   0
#if _PRINT_ALLOC_
#define print_alloc printf
#else
#define print_alloc(...)
#endif

void *osal_malloc(size_t size)
{
    void * ptr = malloc(size);
    print_alloc("malloc prt: %p, size: %d\n", ptr, size);
    return ptr;
}

void *osal_calloc(int num, size_t size)
{
    void * ptr = malloc(size * num);
    print_alloc("calloc prt: %p, size: %d\n", ptr, size);
    if (ptr) {
        memset(ptr, 0, size * num);
    }
    return ptr;
}

void *osal_realloc(void * buf, size_t size)
{
    void * ptr = realloc(buf, size);
    print_alloc("realloc prt: %p, size: %d\n", ptr, size);
    return ptr;
}

void  osal_free(void *ptr)
{
    if (ptr) {
        print_alloc("free prt: %p\n", ptr);
        free(ptr);
    }
}

