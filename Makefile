
DIRS := $(shell find . -maxdepth 3 -type d)
HEAD_FILES = $(foreach dir,$(DIRS),$(wildcard $(dir)/*.h))

.PHONY: all
all:
	cp $(HEAD_FILES) $(OUT_DIR)/include

.PHONY: clean
clean:
