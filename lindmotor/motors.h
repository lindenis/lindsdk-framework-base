#ifndef __MOTORS_H__
#define __MOTORS_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef enum motor_direction {
	MOTOR_DIRECTION_INVALID = 0,
	MOTOR_DIRECTION_CW,
	MOTOR_DIRECTION_CCW,
}motor_direction_t;

/* 函数：MotorsCreate
 * 功能：创建马达设备;
 * 参数：无;
 * 返回值： 0： 成功； -1：失败;
 * 备注:在开机时调用一次即可。
 */
int MotorsCreate (void);

/* 函数：MotorsDestroy
 * 功能：销毁马达设备;
 * 参数：无;
 * 返回值： 无;
 * 备注:在关机时调用一次即可。
 */
void MotorsDestroy (void);

/* 函数：MotorsOpen
 * 功能：申请并初始化马达硬件系统资源;
 * 参数：id: 马达ID;
		 forward: 指定马达旋转的正方向，如MOTOR_DIRECTION_CCW表示马达逆时针旋转为正转;
 * 返回值： 0： 成功； -1：失败
 */
int MotorsOpen (unsigned int id, motor_direction_t forward);

/*
 * 函数：MotorsClose
 * 功能：释放马达硬件系统资源;
 * 参数：id: 马达ID;
 * 返回值：0： 成功； -1：失败
 */
int MotorsClose (unsigned int id);

/*
 * 函数：MotorsSetSpeed
 * 功能：设定马达的运行速度;
 * 参数：id：马达ID;
 *       speed_percent：速度百分比，范围：[0,100]
 * 返回值： 0： 成功； -1：失败
 */
int MotorsSetSpeed(unsigned int id, unsigned int speed_percent);

/*
 * 函数：MotorsForward
 * 功能：马达正转;
 * 参数：id: 马达ID;
 * 		 steps： 马达正转的步数;
 * 返回值： 0： 成功； -1：失败
 */
int MotorsForward(unsigned int id, unsigned int steps);

/*
 * 函数：MotorsBackward
 * 功能：马达反转;
 * 参数：id: 马达ID;
 * 		 steps： 马达反转的步数;
 * 返回值： 0： 成功； -1：失败
 */
int MotorsBackward(unsigned int id, unsigned int steps);

/*
 * 函数：MotorsStop
 * 功能：暂停马达运行；
 * 参数：id：马达ID;
 * 返回值： 0： 成功； -1：失败
 */
int MotorsStop(unsigned int id);

#ifdef __cplusplus
}
#endif

#endif
